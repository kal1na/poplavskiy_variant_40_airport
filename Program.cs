﻿using System;
using System.Collections.Generic;
using System.IO;

namespace poplavskiy_variant_40_airport
{
    class Plane
    {
        static int id { get; set; }
        public int Key { get; set; }
        public string Name { get; set; }
        string Category { get; set; }
        public int Plases { get; set; }
        public Plane(string name,int plases,string category)
        {
            this.Plases = plases;
            this.Category = category;
            this.Name = name;
            this.Key = id++;
        }
        public string Information()
        {
            string info = Key + ":" + Name + ":" + Plases + ":" + Category;
            return info;
        }
    }
    class Flight
    {
        static int id { get; set; }
        public int Key { get; set; }
        public int FlightNumber { get; set; }
        string PlaneName { get; set; }
        int FreePlases { get; set; }
        public string TimeToStart { get; set; }
        int Price { get; set; }
        public Flight(int flightnumber,string planename,int plases,string timetostart,int price)
        {
            this.FreePlases = plases;
            this.TimeToStart = timetostart;
            this.PlaneName = planename;
            this.Key = id++;
            this.FlightNumber = flightnumber;
            this.Price = price;
        }
        public string Information()
        {
            string info = Key + ":" + FlightNumber + ":" + PlaneName + ":" + FreePlases + ":" + TimeToStart + ":" + Price;
            return info;
        }
    }
    class Ticket
    {
        static int id { get; set; }
        public int Key { get; set; }
        int CashboxNumber { get; set; }
        int FlightNumber { get; set; }
        string Date { get; set; }
        string Time { get; set; }
        public Ticket(int cashboxnumber,int flightnumber,string date,string time)
        {
            this.Time = time;
            this.Date = date;
            this.Key = id++;
            this.FlightNumber = flightnumber;
            this.CashboxNumber = cashboxnumber;
        }
        public string Information()
        {
            string info = Key + ":" + CashboxNumber + ":" + FlightNumber + ":" + Date + ":" + Time;
            return info;
        }
    }
    class Program
    {
        static Random rnd = new Random();
        static void Main(string[] args)
        {
            string TicketPath = @"/home/kalin/Documents/c#/Poplavskiy_Kursovaya_Variant_40_Airport/poplavskiy_variant_40_airport/bd/Ticket.txt";
            string FlightPath = @"/home/kalin/Documents/c#/Poplavskiy_Kursovaya_Variant_40_Airport/poplavskiy_variant_40_airport/bd/Flight.txt";
            string PlanePath = @"/home/kalin/Documents/c#/Poplavskiy_Kursovaya_Variant_40_Airport/poplavskiy_variant_40_airport/bd/Plane.txt";
            List<Plane> PlaneList = new List<Plane>();
            Console.WriteLine("Plane count: ");
            int PlaneCount = Convert.ToInt32(Console.ReadLine());
            for(int i = 0; i < PlaneCount; i++)
            {
                PlaneList.Add(new Plane(GetPlaneName(),rnd.Next(200,300),GetCategory()));
            }
            List<Flight> FlightList = new List<Flight>();
            Console.WriteLine("Flight count: ");
            int FlightCount = Convert.ToInt32(Console.ReadLine());
            for(int i = 0; i < FlightCount; i++)
            {
                int ThisPlane = rnd.Next(0,PlaneCount);
                FlightList.Add(new Flight(i, PlaneList[ThisPlane].Name, rnd.Next(0,PlaneList[ThisPlane].Plases),GetFlightTime(),GetFlightPrice()));
            }
            List<Ticket> TicketList = new List<Ticket>();
            for(int i = 0; i < FlightCount; i++)
            {
                string[] days = new string[31]; 
                for(int o = 0; o < 31; o++)
                {
                    days[o] = Convert.ToString(o);
                }
                string day = days[rnd.Next(0,30)];    
                for(int j = 0; j < PlaneList[i].Plases; j++)
                {
                    TicketList.Add(new Ticket(rnd.Next(0,20),FlightList[i].FlightNumber,day,FlightList[i].TimeToStart));
                }
            }
            using (StreamWriter sw = new StreamWriter(PlanePath, false, System.Text.Encoding.Default))
            {
                foreach(Plane p in PlaneList)
                {
                    sw.WriteLine(p.Information());
                }
            }
            using (StreamWriter sw = new StreamWriter(FlightPath, false, System.Text.Encoding.Default))
            {
                foreach(Flight f in FlightList)
                {
                    sw.WriteLine(f.Information());
                }
            }
            using (StreamWriter sw = new StreamWriter(TicketPath, false, System.Text.Encoding.Default))
            {
                foreach(Ticket t in TicketList)
                {
                    sw.WriteLine(t.Information());
                }
            }
        }
        static string GetFlightTime()
        {
            string[] Time = new string[]{"00-00","02-00","04-00","06-00","09-30","12-15","16-40","20-00","21-30","23-00"};
            return Time[rnd.Next(0,10)];
        }
        static int GetFlightPrice()
        {
            return Convert.ToInt32(Convert.ToString(rnd.Next(2,10)) + "000");
        }
        static string GetCategory()
        {
            string[] cat = new string[] {"VIP","A","B","C"};
            return cat[rnd.Next(0,4)];
        }
        static string GetPlaneName()
        {
        string[] names = new string[] {"ТУ204","ТУ204","ТУ154","МС21","АН148"};
        return names[rnd.Next(0,5)];
        }
    }
}
